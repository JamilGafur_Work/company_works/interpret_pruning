import torch
import numpy as np
import tqdm
import pdb

def get_trainable_weights(model, layer_type=torch.nn.Conv2d):
    trainable_weights = []
    # go through every module in the model
    for module in model.modules():
        # check if the module is an instance of the layer_type class
        if isinstance(module, layer_type):
            for name, param in module.named_parameters():
                if param.requires_grad:
                    trainable_weights.append(param.flatten().detach())
    trainable_weights = torch.cat(trainable_weights)
    if layer_type == torch.nn.Linear:
        # get the number of trainable weights in the last layer and remove them from the mask
        last_layer_trainable_weights = list(model.named_parameters())[-1][1].flatten().detach()
        num_last_layer_trainable_weights = len(last_layer_trainable_weights)

        return_weights = trainable_weights[:-num_last_layer_trainable_weights]
        return return_weights
    else:
        return trainable_weights


def update_weights(model, mask, layer_type=torch.nn.Conv2d):
    with torch.no_grad():
        idx = 0
        for module in model.modules():
            if isinstance(module, layer_type) and 'bias' not in module._get_name() and module.weight.requires_grad:
                module.weight.data *= mask[idx]
                idx += 1


def update_mask(model, mask, prune_percent, layer_type):
    current_weights = get_trainable_weights(model, layer_type)
    threshold = np.quantile(np.abs(current_weights), prune_percent)
    new_mask = np.abs(current_weights) >= threshold
    mask *= new_mask
    return mask

def train_one_epoch(model, train_loader, criterion, optimizer, device):
    model.train()
    running_loss = 0.0
    #  remove the tqdm once done

    for inputs, targets in tqdm.tqdm(train_loader, total=len(train_loader), desc='Training', leave=False):
        inputs, targets = inputs.to(device), targets.to(device)
        optimizer.zero_grad()
        outputs = model(inputs)
        loss = criterion(outputs, targets)
        loss.backward()
        optimizer.step()
        running_loss += loss.item() * inputs.size(0)
    epoch_loss = running_loss / len(train_loader.dataset)
    return epoch_loss

def iterative_prune(model, epochs, train_loader, test_loader, criterion, optimizer, device, prune_percent=0.2, layer_type=None):
    mask = torch.ones_like(get_trainable_weights(model, layer_type))
    
    prune_percent_per_epoch = prune_percent / epochs
    total_prune = prune_percent_per_epoch
    
    for epoch in range(epochs):
        # Train the model
        train_loss = train_one_epoch(model, train_loader, criterion, optimizer, device)
        
        # Update the mask
        mask = update_mask(model, mask, total_prune, layer_type)
        update_weights(model, mask, layer_type)
        
        print(f"percent pruned: {total_prune}")
        print(f"actual percent pruned: {sum(mask==0)/len(mask)}\n\n")

        assert torch.isclose(torch.tensor(total_prune), sum(mask==0)/len(mask), .001), 'Total prune percentage does not match mask'

        # Update the prune percentage
        total_prune += prune_percent_per_epoch
        


def test_model(model, test_loader, device):
    # Evaluate the pruned model on the test set
    model.eval()
    with torch.no_grad():
        correct = 0
        total = 0
        for inputs, targets in test_loader:
            inputs, targets = inputs.to(device), targets.to(device)
            outputs = model(inputs)
            _, predicted = torch.max(outputs, 1)
            total += targets.size(0)
            correct += (predicted == targets).sum().item()
        test_acc = 100 * correct / total
        print('Test Accuracy: {:.2f}%'.format(test_acc))
     