import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torchvision.datasets import MNIST
from torchvision.transforms import ToTensor
from pruner import *

def getModel():
    return   nn.Sequential(
    nn.Conv2d(1, 32, kernel_size=3, padding=1),
    nn.ReLU(),
    nn.Conv2d(32, 64, kernel_size=3, padding=1),
    nn.ReLU(),
    nn.MaxPool2d(2),
    nn.Flatten(),
    nn.Linear(64*14*14, 128),
    nn.ReLU(),
    nn.Linear(128, 10)
    )

def getData(batch_size):
    # Download the MNIST DATASET and create a PyTorch dataloader
    train_dataset = MNIST(root="data/", train=True, transform=ToTensor(), download=True)
    #  keep only half of the dataset
    train_dataset = torch.utils.data.Subset(train_dataset, range(0, len(train_dataset), 2))

    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)

    test_dataset = MNIST(root="data/", train=False, transform=ToTensor(), download=True)
    #  keep only half of the dataset
    test_dataset = torch.utils.data.Subset(test_dataset, range(0, len(test_dataset), 2))
    test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False)

    return train_loader, test_loader


def main():
    batch_size = 1024
    # Create a model 
    model = getModel()
    initalWeights = model.state_dict()

    # get data
    train_loader, test_loader = getData(batch_size)

    prune_percents = [0, .99]
    epochs = 3
    pruneEpochs = 3
    Study = {}
    for prune_percent in prune_percents:
        print(f"prune_percent: {prune_percent}")
        model = getModel()
        model.load_state_dict(initalWeights)
            
        # Define the loss function, optimizer and device
        criterion = nn.CrossEntropyLoss()
        optimizer = torch.optim.SGD(model.parameters(), lr=0.1)
        device = 'cpu' if not torch.cuda.is_available() else 'cuda:0'

        # initial train
        # for _ in range(epochs):
        #     train_one_epoch(model, train_loader, criterion, optimizer, device)

        # iterative prune
        iterative_prune(model, pruneEpochs, train_loader, test_loader, criterion, optimizer, device, prune_percent=prune_percent, layer_type=torch.nn.Conv2d)
        # test
        test_model(model, test_loader, device)
        Study[prune_percent] = model.state_dict()
        # save the model to disk
        torch.save(model.state_dict(), f"model_{prune_percent}.pth")
        print("\n\n\n")

if __name__ == '__main__':
    main()